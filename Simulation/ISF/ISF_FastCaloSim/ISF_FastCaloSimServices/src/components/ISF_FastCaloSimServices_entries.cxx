#include "../DNNCaloSimSvc.h"
#include "../FastCaloSimV2ParamSvc.h"
#include "../FastCaloSimSvcV2.h"
#include "../FastCaloSimSvcPU.h"
#include "../FastCaloTool.h"

DECLARE_COMPONENT( ISF::DNNCaloSimSvc )
DECLARE_COMPONENT( ISF::FastCaloSimV2ParamSvc )
DECLARE_COMPONENT( ISF::FastCaloSimSvcV2 )
DECLARE_COMPONENT( ISF::FastCaloSimSvcPU )
DECLARE_COMPONENT( ISF::FastCaloTool )
