#include "TrigFTKFastSimTruth/TrigFTKFastSimTruth.h"
#include "TrigFTKFastSimTruth/TrigFTKSectorMatchTool.h"
#include "TrigFTKFastSimTruth/TrigFTKTrackBuilderTool.h"

DECLARE_COMPONENT( TrigFTKFastSimTruth )
DECLARE_COMPONENT( TrigFTKSectorMatchTool )
DECLARE_COMPONENT( TrigFTKTrackBuilderTool )
