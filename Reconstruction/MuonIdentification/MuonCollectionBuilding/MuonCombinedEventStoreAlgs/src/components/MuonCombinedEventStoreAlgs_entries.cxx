#include "../FinalizeMuonContainers.h"
#include "../InitializeMuonClusters.h"
#include "../FinalizeMuonClusters.h"
#include "../MergeMuonCaloClusterContainers.h"
#include "../MuonToTrackConverterAlg.h"

DECLARE_COMPONENT( Rec::FinalizeMuonContainers )
DECLARE_COMPONENT( Rec::InitializeMuonClusters )
DECLARE_COMPONENT( Rec::FinalizeMuonClusters )
DECLARE_COMPONENT( Rec::MergeMuonCaloClusterContainers )
DECLARE_COMPONENT( MuonToTrackConverterAlg )

